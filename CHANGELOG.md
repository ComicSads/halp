# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

If you see an X in a date/future version, it means it hasn't been decided upon yet.

## [Unreleased/Todo]
Possible topics are Added, Changed, Deprecated, Removed, Fixed, Security
Note: Just because it's here, doesn't mean it's 100% going to happen. This section is also used as a todo first and foremost. You are encouraged to rewrite descriptions to be less vague as you move them into the actual Changelog.

- Config option to keep program running after a choice has been made
- Config option to use a certain browser
- Specify an argument to use on command line
- A cleaner interface
- Options now have numbers you can specify that change depending on how many options there are.
- Have script work better on slow internet, probably by using &
- Better debugging tools
- Create a function for case responses, as most are the same.
- Notify the user if they request a choice that isn't available, and (possibly) prompt them before opening the choice.



## [0.2.0] - 2019-04-17 # Current next numbered release
### Added
- Config options available as command line arguments
- Config option to specify a text output program
- Config option to use just the default output for each program
- Cleaner comments
- Version information
### Fixed
- Arch wiki now only shows up as an option when it has a page for the requested command
- Exits automatically if no option has help

## [0.1.0] - 2019-03-26
### Added
- Complete functionality for cheatsheets, manpages, bropages, cht.sh. Partial functionality for Arch Wiki.
- Whatis output 
- Optional internet functionality
