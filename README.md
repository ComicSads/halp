Welcome to a script that parses through all of the ways to get help with a Linux command (cheat, man, bropages, etc.) and let you see what ones have answers for that command.
If something isn't working exactly right, create an issue, or a pull request with a fix.
If you want another method to be added, create an issue, or a pull request with it added.
If you're gonna make a contribution, please add it to the develop branch, and at some point it will be merged into master.

Dependencies: Basically None
Bash
curl if you want anything off the internet
xdg-open is used to open websites
tr (GNU coreutils)
