#!/bin/sh
# shellcheck disable=1117
default=/etc/halp/defaults # Either symlink the defaults file here, or change the path. Open to suggestions for other locations to store the file
version="0.2.0"

while getopts ":ho:v" opt; do # Get options 
	case ${opt} in
		o ) # -o is used to specify a configuration option e.g. internet yes or no
			options="$options\n$OPTARG"
			;;
		: ) # Output if -o isn't given a configuration option
			echo "Invalid option: $OPTARG requires an option"
			;;
		\? ) # Output if an invalid option is passed to halp
			echo "Invalid option: if you need help, run 'halp -h'"
			;;
		h ) # Basic help example
			echo "Usage: halp [-o OPTIONS=value] [command]"
			;;
		v ) # Version information
			echo "Halp is version $version"
			exit
	esac
done
shift $((OPTIND -1)) # Shift the arguments so we have just the one necessary for finding help
args="$*"

options=$(echo "$options" | tail -n +2) # Remove the leading empty line

# Comply with XDG filesystem
if [ -z "$XDG_CONFIG_HOME" ]; then # Configuration directory set to ~/.config by default
	config="$HOME"/.config # I'm too lazy to hit shift to type in the full XDG variable name, so just set it to $config
else
	config="$XDG_CONFIG_HOME" 
fi 
#I don't think this code is necessary since I moved the default file to /etc/halp
#if [ ! -d "$config"/halp ];then # Create a halp directory if there isn't one
#	mkdir -p "$config"/halp
#fi 

if [ -f $default ];then # If the default config file exists, we source it.
	#shellcheck source=defaults
	. $default 
fi
if [ -f "$config"/halp/config ];then # If the user configuration file exists, we source it.
	#shellcheck source=/dev/null
	. "$config"/halp/config 
fi
if [ -n "$options" ]; then # If any options were set as command line options, we source them
	options="$(echo "$options" | sed 's/^/export /')" # This sed command adds export to the start of each -o option given to the script
	$options
fi

possible="exit" # Begin seeing what options are available for help

# Identify if a man page exists
man "$args" > /dev/null 2>&1 && possible="$possible:man"
# Each choice is delimited by a :

# Identify if a cheat page exists
if [ "$(command -v cheat)" ]; then
	cheater=$(cheat "$args" 2>&1 | grep "No cheatsheet found for") # Search for a line containing the cheat no sheet found
	if [ -z "$cheater" ]; then # If no lines were found
		possible="$possible:cheat" # Add cheat as a choice
	fi
fi

# Internet options section
if [ "$internet" = on ]; then
	# Identify if bro is installed
	if [ ! -z "$(command -v bro)" ]; then
		# Identify if a bropage exists
		broing="$(curl -s "bropages.org/$args")"
		broing="$(echo "$broing" | grep -o 'Page')" # Bropages will give a html page with the text "Page Not Found" if there isn't anything
		if [ -z "$broing" ]; then # If no line is found, add bro as an option
			possible="$possible:bro"
		fi
	fi
	# Check if there is a page on the arch wiki 
	arch="$(curl -s "https://wiki.archlinux.org/index.php/$args")"
	if [ -n "$arch" ]; then
		arch=$(echo $arch | grep 'There is currently no text in this page')
		if [ -z "$arch" ]; then
			possible="$possible:arch"
		fi
	fi
	# Check if there is a page on cht.sh for help
	cht="$(curl -s "https://cht.sh/$args" | grep 'Do you mean one of these topics maybe?')"
	if [ -z "$cht" ]; then
		possible="$possible:cht"
	fi
fi

# If no help pages were found, print a message and exit the program
if [ $possible = "exit" ]; then
	echo "No help found"
	echo "Try using $args --help"
	# If whatis has anything to say, post it anyway
	if whatis "$args" 2>&1 | grep -vq " nothing appropriate"; then 
		whatis "$args"
	fi
	exit
fi

# Post whatis output
whatis "$args"

# Print all options with newlines instead of :
echo "$possible" | tr ":" "\n"  
read -r answer
while true
do
	# Users can specifically ask for a choice that isn't in help, however, this isn't likely to cause a problem, seeing how most people would only do that if they knew the program was wrong.
	case $answer in
		# For the sake of brevity and cleanliness, Generic comments will be on man) only, with any specicial things to note being added for each program
		man)
			if [ -z $output ]; then         # If an output configuration has not been set
				man "$args"             # Just run the command as normal
			else                            # Otherwise
				man "$args" | $output   # Print the output of the command through the output
			fi
			break
			;;
		cheat)
			if [ -z $output ]; then
				cheat "$args"
			else
				cheat "$args" | $output
			fi
			break
			;;
		bro) # Bro and Arch have specific methods of pulling data, so they don't care about what $output is set to
			bro "$args" 
			break
			;;
		arch)
			xdg-open "https://wiki.archlinux.org/index.php/$args"
			break
			;;
		cht)
			if [ -z $output ]; then
				curl "https://cht.sh/$args" 
			else
				curl "https://cht.sh/$args" | $output
			fi
			break
			;;
		exit)
			break
			;;
		*) # If the user requested an option that isn't available, or mistyped something, print out the options that are available again.
			echo "$possible" | tr ":" "\n"  
			read -r answer
			;;
	esac
done
